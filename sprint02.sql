DROP TABLE IF EXISTS `note`;
CREATE TABLE IF NOT EXISTS `note` (
	`id_note` int NOT NULL AUTO_INCREMENT,
	`type` enum('Texte','Audio') NOT NULL DEFAULT 'Texte',
	`description` longtext,
	`lien_audio` varchar(250),
	`date_creation` datetime NOT NULL,
	PRIMARY KEY (`id_note`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `action`;
CREATE TABLE IF NOT EXISTS `action` (
	`id_action` int NOT NULL AUTO_INCREMENT,
	`frequence` int NOT NULL DEFAULT 1,
	`date_debut` datetime,
	`date_fin` datetime,
	`status` enum('A faire','Realisé','Abandonné') NOT NULL DEFAULT 'A faire',
	PRIMARY KEY (`id_action`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


PREPARE alterIfNotExists FROM (
	SELECT IF (
	SELECT count(*) FROM INFORMATION_SCHEMA.COLUMNS
	WHERE
		(table_name = 'zone')
	AND (column_name = 'latitude')
	AND (column_name = 'longitude')
	AND (column_name = 'largeur')
	AND (column_name = 'hauteur')
	) > 0,
	"SELECT 1",
	CONCAT(
		ALTER TABLE `zone` ADD (
			`latitude` float(10,6),
			`longitude` float(10,6),
			`largeur` int,
			`hauteur` int
		);
	)
);
EXECUTE alterZoneIfNotExists;
DEALLOCATE PREPARE alteIfNotExists;

ALTER TABLE `plante` ADD (
	`temperature_min` int
);