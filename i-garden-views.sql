--
-- Vue contenant le nom de la plante, sa zone et son cycle végétatif actuel
--

CREATE OR REPLACE VIEW Liste_des_plantes AS
SELECT 
	g.nom AS 'nom',
	z.nom AS 'zone',
	cv.nom AS 'cycle_vegetatif'
FROM 
	graine AS g
INNER JOIN
	plante AS p ON p.id_graine = g.id_graine
INNER JOIN
	zone AS z ON z.id_zone = p.id_zone
INNER JOIN
	cycle_vegetatif AS cv ON cv.id_cycle_vegetatif = p.id_cycle_vegetatif
WHERE
	g.nom IS NOT NULL
AND
	z.nom IS NOT NULL
AND
	cv.nom IS NOT NULL;

--
-- Vue contenant la liste des plantes sans zone
--

CREATE OR REPLACE VIEW Plantes_hors_zone AS
SELECT
	g.nom AS 'nom',
	u.pseudo AS 'proprietaire'
FROM
	graine AS g, plante AS p
LEFT JOIN
	jardin_utilisateur AS ju ON ju.id_jardin = p.id_jardin
INNER JOIN
	utilisateur AS u ON u.id_utilisateur = ju.id_utilisateur
WHERE
	p.id_zone IS NULL;

--
-- Vue contenant la liste des zones, son code de rusticité et le nombre de plantes dessus
--

CREATE OR REPLACE VIEW Zones_recap AS
SELECT
	z.nom AS 'nom',
	r.code_rusticite,
	COUNT(p.id_plante) AS nombre_plante
FROM
	zone AS z
LEFT JOIN
	rusticite AS r ON r.id_rusticite = z.id_rusticite
LEFT JOIN
	plante AS p ON p.id_zone = z.id_zone;

--
-- Vue contenant liste des graines avec son nom, son genre et son espece et son nom en latin
--

CREATE OR REPLACE VIEW Plantes_recap AS
SELECT
	g.nom,
	f2.nom AS 'genre',
	f3.nom AS 'espece',
	g.nom_latin
FROM
	graine AS g
LEFT JOIN
	famille AS f1 ON f1.id_famille = g.id_famille
LEFT JOIN
	famille AS f2 ON f2.id_famille = f1.id_famille_parente
LEFT JOIN
	famille AS f3 ON f3.id_famille = f2.id_famille_parente;

--
-- Vue contenant la liste des plantes en situations précaires
--

CREATE OR REPLACE VIEW Plantes_precaires AS 
SELECT
	p.temperature_min,
	r.temp_min,
	g.nom,
	g.nom_latin
FROM 
	plante AS p
LEFT JOIN 
	rusticite AS r ON r.temp_min = p.temperature_min
LEFT JOIN
	graine as g ON p.graine_id = g.graine_id
WHERE 
	p.temperature_min < r.temp_min;

	
	
	
