CREATE TRIGGER status_onchange_etat_plante AFTER UPDATE ON etat_plante FOR EACH ROW
INSERT INTO historique_etat
VALUES(NEW.id_etat, NEW.id_plante, NOW(), NEW.date, NEW.status);

DELIMITER |
CREATE TRIGGER cycle_onchange_cycle_plante AFTER UPDATE ON plante FOR EACH ROW
BEGIN
	IF OLD.id_cycle_vegetatif != NEW.id_cycle_vegetatif
		THEN INSERT INTO historique_cycle VALUES(OLD.id_cycle_vegetatif, OLD.id_plante, NOW());
	END IF;
END |
DELIMITER ;


DELIMITER |
CREATE TRIGGER cycle_onchange_zone_plante AFTER UPDATE ON plante FOR EACH ROW
BEGIN
	IF OLD.id_zone != NEW.id_zone
		THEN INSERT INTO historique_cycle VALUES(OLD.id_plante, OLD.id_zone, NOW());
	END IF;
END |
DELIMITER ;
