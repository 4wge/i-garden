ALTER TABLE `plante` DROP FOREIGN KEY `fk_cycle_vegetatif_plante`; ALTER TABLE `plante` 
ADD CONSTRAINT  `fk_cycle_vegetatif_plante` FOREIGN KEY (`id_cycle_vegetatif`) 
REFERENCES `cycle_vegetatif`(`id_cycle_vegetatif`) 
ON DELETE CASCADE ON UPDATE RESTRICT; ALTER TABLE `plante` 
DROP FOREIGN KEY `fk_zone_plante`; ALTER TABLE `plante` ADD CONSTRAINT `fk_zone_plante` FOREIGN KEY (`id_zone`) REFERENCES `zone`(`id_zone`) ON DELETE CASCADE ON UPDATE RESTRICT;

CREATE INDEX nom_famille ON famille (nom)

